package com.example.app3.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Gas(@StringRes val stringResourcePrice: Int, @StringRes val StringResourceTitle: Int)
